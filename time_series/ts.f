        implicit none
        integer N
        parameter(N=5)

        external derivs, lsoda

        double precision y(N), step
        double precision alpha, beta, gama, delta, lambdau, lambdal, mu,
     *                b, c, d, h, k, l,  r, sigma
        double precision sigma1, sigma2

c       lsoda variables
        integer itol, itask, istate, iopt, lrw, iwork(20+N)
        integer liw, jt, i, mesflg, lunit

        double precision rtol, atol, rwork(22 + N*16), jdum, jac
        double precision t, tf

          common/cte/ alpha, beta, gama, delta, lambdau, lambdal, mu,
     *                b, c, d, h, k, l,  r, sigma, sigma1, sigma2
        common/eh0001/ mesflg, lunit

        open(1, status='unknown', file = 'time_series.dat')

c       Initial conditions
        y(1) = 1.e0
        y(2) = 1.e0
        y(3) = 1.e0
        y(4) = 1.e0
        y(5) = 1.e0

c       Parameter values
        lambdau = 1.0
        mu = 0.1
        gama = 0.01
        r = 1.0
        d = 0.001
        k = 0.01
        lambdal = 5.0
        b = 1.0
        delta = 0.01
        sigma = 0.1
        c = 1.0
        h = 0.01
        alpha = 2.0
        beta = 1.0
        sigma1 = 0.63
        sigma2 = 1.0

c       lsoda variables
        mesflg = 1
        ITOL=1
        RTOL= 1.0D-10
        ATOL= 1.0D-10
        ITASK=1
        ISTATE=1
        IOPT=0
        LRW = (22+n*(16))
        LIW = 20+n
        JT=2
        do i=5,10
          iwork(i) = 0
          rwork(i) = 0.0
        enddo
        iwork(6) = 10000

        t = 0.
        step = 0.001

c       Transients loop

        do while(t.lt.2000)

          tf = t + step

          call lsoda(derivs, n, y, t, tf, itol, rtol, atol, itask,
     *               istate, iopt, rwork, lrw, iwork, liw,
     *               jac, jt)

        enddo


        do while(t.lt.3000.0)

          tf = t + step

          call lsoda(derivs, n, y, t, tf, itol, rtol, atol, itask,
     *               istate, iopt, rwork, lrw, iwork, liw,
     *               jac, jt)

          write(1, *) t, y(1), y(2), y(3), y(4), y(5)

        enddo

        end

        subroutine derivs(nn, t, y, yprime)
          implicit none
          integer nn
          double precision t, y(nn), yprime(nn)
        double precision alpha, beta, gama, delta, lambdau, lambdal, mu,
     *                b, c, d, h, k, l,  r, sigma
        double precision sigma1, sigma2

          common/cte/ alpha, beta, gama, delta, lambdau, lambdal, mu,
     *                b, c, d, h, k, l,  r, sigma, sigma1, sigma2

          yprime(1) = lambdau - mu*y(1) - gama*y(1)*y(4)

          yprime(2) = gama*y(1)*y(4) + r*y(2) - beta*y(2)*y(4)
     *                 - d*y(2)*y(2) + k*y(5)*y(2)

          yprime(3) = sigma1*beta*y(2)*y(4) - alpha*y(3)

          yprime(4) = sigma2*lambdal*alpha*y(3) - b*y(4)
     *                    - delta*(y(2) + y(3))*y(4) - sigma*y(2)*y(4)

          yprime(5) = c*y(5) - h*y(5)*y(2)

        end
